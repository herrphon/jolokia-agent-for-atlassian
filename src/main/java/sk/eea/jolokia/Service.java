package sk.eea.jolokia;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import org.jolokia.restrictor.PolicyRestrictor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/access")
public class Service {
    public static final String KEY = "plugin.jolokia.access.xml";
    private final PluginSettingsFactory pluginSettingsFactory;
    private final UserManager userManager;
    private final TransactionTemplate transactionTemplate;


    public Service(PluginSettingsFactory pluginSettingsFactory, UserManager userManager, TransactionTemplate transactionTemplate) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.userManager = userManager;
        this.transactionTemplate = transactionTemplate;
    }

    public PolicyRestrictor getStoredPolicyRestrictor() {
        String access_xml = (String) pluginSettingsFactory.createGlobalSettings().get(KEY);
        if (access_xml != null) {
            try {
                return new PolicyRestrictor(new ByteArrayInputStream(access_xml.getBytes(StandardCharsets.UTF_8)));
            } catch (SecurityException e) {
                // FIXME log about invalid entry in settings
            }
        }
        return null;
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public Response get(@Context HttpServletRequest request) {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                // TODO return default content
                Object access_xml = settings.get(KEY);
                return access_xml != null ? access_xml : "";
            }
        })).build();
    }

    @PUT
    @Consumes(MediaType.TEXT_XML)
    public Response put(final String access_xml, @Context HttpServletRequest request) {
        String username = userManager.getRemoteUsername(request);
        if (username == null || !userManager.isSystemAdmin(username)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        if ("".equals(access_xml)) {
            // reseting on empty value
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    pluginSettingsFactory.createGlobalSettings().remove(KEY);
                    return null;
                }
            });

        } else {
            // parsing provided configuration
            try {
                new PolicyRestrictor(new ByteArrayInputStream(access_xml.getBytes(StandardCharsets.UTF_8)));
            } catch (SecurityException e) {
                return Response.serverError().build();
            }
            transactionTemplate.execute(new TransactionCallback() {
                public Object doInTransaction() {
                    pluginSettingsFactory.createGlobalSettings().put(KEY, access_xml);
                    return null;
                }
            });
        }
        return Response.noContent().build();
    }

}