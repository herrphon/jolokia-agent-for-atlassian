package sk.eea.jolokia;


import org.jolokia.http.AgentServlet;
import org.jolokia.restrictor.Restrictor;

public class Servlet extends AgentServlet {

    private final Service service;

    public Servlet(Service service) {
        this.service = service;

    }

    @Override
    protected Restrictor createRestrictor(String pLocation) {
        Restrictor restrictor = service.getStoredPolicyRestrictor();
        return restrictor != null ? restrictor : super.createRestrictor(pLocation);
    }
}
