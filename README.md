Jolokia Agent packaged for Atlassian products as P2 add-on.

Agent provides HTTP endpoint for JMX at <APP_BASE>/plugins/servlet/jolokia

Add-on is installable using UPM. Default 'jolokia-access.xml' allows access from localhost for read, list and version operation.
You can change this definition over REST as system administrator at <APP_BASE>/rest/jolokia/1.0/access.

